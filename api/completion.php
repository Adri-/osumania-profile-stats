<?php
require_once '/var/track/api/lib/TrackDataManager.php';
$dm = new DataManager();

$min = 0;
$max = 100;

?>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
</head>
<div style='text-align: center;margin-top:50px;margin-top: 70px;'>
    <div class='bodycontainer' style='width:70%;margin-right:auto;margin-left:auto;'>
        <br>
        <p style='font-size:220%'>PP graphs</p>
        <div id='graphusage' style='width: 98%; height: 400px;margin: auto auto auto auto;'></div>
    </div>

    <script>
        function createGraphs() {
                Plotly.newPlot('graphusage', <?php echo $dm->getCompletion($min,$max); ?>);
        }
    </script>

    <script>
        $(document).ready(function () {
            createGraphs();
        })
    </script>
</div>