<?php
header('Access-Control-Allow-Origin: *');

if (!isset($_REQUEST["k"])) {
    echo json_encode(["missing_param" => "You need to provide an API key."]);
    die();
}
if (!isset($_REQUEST["u"])) {
    echo json_encode(["missing_param" => "You need to provide a user ID."]);
    die();
}
if (!isset($_REQUEST["m"]) || !is_numeric($_REQUEST["m"])) {
    $_REQUEST["m"] = "both";
}
if (!isset($_REQUEST["s"])) {
    $_REQUEST["s"] = "0";
}

include './lib/TrackAPIManager.php';

$api = new APIManager();

$access = $api->authorize($_REQUEST["k"], "import");

if (isset($access["error"])) {
    echo json_encode($access);
    die();
}

$dm = new DataManager();

if(!$_REQUEST["s"]) {
    $info = $dm->getUserInfo($_REQUEST["u"]);
}
if ($info) {
    echo json_encode(["error" => "Already Registred"]);
} else {
    $info = $dm->newUser($_REQUEST["u"],$_REQUEST["s"]);

    if($info != null) {
        /*
        if($_REQUEST["m"] == "both") {
            $dm->importScoresFromOsudaily($info[1], 4);
            $dm->importScoresFromOsudaily($info[1], 7);
        } else if ($_REQUEST["m"] == "4") {
            // I don't trust requested parameters
            $dm->importScoresFromOsudaily($info[1], 4);
        } else if ($_REQUEST["m"] == "7") {
            $dm->importScoresFromOsudaily($info[1], 7);
        }
        */
        $dm->addRecent($info[1]);
        $dm->addBest($info[1]);

        $dm->recalcPP($info[1],4);
        $dm->recalcPP($info[1],7);

        $dm->recalcRank(4);
        $dm->recalcRank(7);

        $dm->recalcCountry(4, $info[0]);
        $dm->recalcCountry(7, $info[0]);

        echo json_encode(["msg" => "You are now tracked !"]);
    } else {
        echo json_encode(["msg" => "Could not find you in the api."]);
    }
}
