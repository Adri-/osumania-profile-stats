<?php
header('Access-Control-Allow-Origin: *');

if(!isset($_REQUEST["k"])) {
    echo json_encode(["missing_param" => "You need to provide an API key."]);
    die();
}

include './lib/TrackAPIManager.php';

$api = new APIManager();

$access = $api->authorize($_REQUEST["k"], "leaderboards");

if(isset($access["error"])) {
    echo json_encode($access);
    die();
}

$dm = new DataManager();

$lb_4k = $dm->getLeaderboard(4);
$lb_7k = $dm->getLeaderboard(7);

$data = [
    "rankings" => [
        4 => $lb_4k,
        7 => $lb_7k,
    ]
];

echo json_encode($data); 