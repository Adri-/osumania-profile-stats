<?php

require_once("/var/osu/TrackAPISecure.php");
require_once("TrackDataManager.php");

class APIManager
{

    private $s;

    public function __construct()
    {
        $this->s = new APISecure();
    }

    public function fast($req, $opt = [])
    {
        return $this->s->fast($req, $opt);
    }

    public function getUniqueKey()
    {
        return $this->s->getUniqueKey();
    }

    public function regen($user)
    {
        return $this->s->regenKey($user);
    }

    public function authorize($key, $scope)
    {
        $keyData = $this->s->getKeyData($key);
        $return = [
            "error" => "Only 1 request per second is authorized"
        ];

        // Verify key
        if (!$keyData) {
            $return = [
                "error" => "Invalid key provided."
            ];
            return $return;
        }
        $keyData = $keyData[0];
        if ($keyData["rate"] == 0) {
            $this->addRequest($key, $scope);
            return $this->performRequest($scope, $keyData);
        } else {
            // Verify ratelimit
            $req = "SELECT `used` FROM ps_key WHERE `key_`=:key";
            $opt = [
                ":key" => $key
            ];
            $rep = $this->fast($req, $opt);

            if (date("Y-m-d H:i:s") != $rep["used"]) {
                $this->addRequest($key, $scope);
                return $this->performRequest($scope, $keyData);
            } else {
                $return = [
                    "error" => "Only 1 request per second is authorized"
                ];
                return $return;
            }
        }
    }

    public function performRequest($scope, $keyData)
    {
        $permission_denied = [
            "error" => "Your key doesn't hold the privilege to access this scope."
        ];
        $authorized = ["authorized" => "1"];
        switch ($scope) {
            case "users":
                return $authorized;
            case "user":
                return $authorized;
            case "update":
                return $authorized;
            case "new":
                return $authorized;
            case "leaderboards":
                return $authorized;
            case "import":
                if ($keyData["level"] > 4) {
                    return $authorized;
                } else {
                    return $permission_denied;
                }
            default:
                return [
                    "error" => "Bad scope : $scope."
                ];
        }
    }

    public function addRequest($key)
    {
        $req = "UPDATE ps_key SET `used` = '" . date("Y-m-d H:i:s") . "' WHERE `key_` = :key";
        $this->fast($req, [":key" => $key]);
    }
}
