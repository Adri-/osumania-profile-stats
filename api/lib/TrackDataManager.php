<?php

require_once("/var/osu/Database.php");
require_once("/var/track/api/osu_api/OsuAPIHandler.php");

class DataManager
{

    public $database;

    public function __construct()
    {

        $this->database = new DatabaseBack();
        $this->api = new OsuAPIHandler();
    }

    public function fast($req, $opt = [])
    {
        return $this->database->fast($req, $opt);
    }

    public function getUsersMin($update = false)
    {
        $c = "";
        if($update) {
            $c = " WHERE last_update < \"".date("Y-m-d H:i:s", time() - 5400)."\"";
        }
        return $this->fast("SELECT id,username FROM ps_user$c");
    }

    public function getUserInfo($id, $s = 0)
    {
        $opt = [":id" => $id];
        if ($s == 0) {
            return $this->fast("SELECT * FROM ps_user WHERE id = :id", $opt);
        } else {
            return $this->fast("SELECT * FROM ps_user WHERE username = :id", $opt);
        }
    }

    public function getUserFull($id)
    {
        $opt = [":id" => $id];
        return $this->fast("SELECT pu.id as osu_id, username, country, registration_date, pp4, pp7, rank4, rank7, rank_country4, rank_country7, lib, short FROM ps_user as pu, osu_country as c WHERE pu.id = :id AND pu.country = c.id", $opt);
    }

    public function getAllUserScores($user, $keycount = "any") {
        if($keycount == "any") {
            return $this->fast("SELECT * FROM ps_score WHERE user = :user", [':user' => $user]);
        } else {
            return $this->fast("SELECT * FROM ps_score as s, osu_allbeatmaps as b WHERE s.map = b.beatmap_id AND mode = 3 AND diff_size = :mode AND user = :user", [':user' => $user, ':mode' => $keycount]);
        }
    }

    public function addScores($id, $scores, $beatmap_id = 0, $ranked = false)
    {
        foreach ($scores as $score) {
            if (!isset($score->{'beatmap_id'}) && $beatmap_id != 0) {
                $score->{'beatmap_id'} = $beatmap_id;
            }
            if (!isset($score->{'user_id'}) && $id != 0) {
                $score->{'user_id'} = $id;
            }
            $map = $this->fast("SELECT approved, diff_size FROM osu_allbeatmaps WHERE beatmap_id = " . $score->{'beatmap_id'});
            if(!$map) {
                continue;
            }
            $map = $map[0];
            if (!$ranked) {
                if (!$map || ($map["approved"] != 1 && $map["approved"] != 2)) {
                    continue;
                }
            }
            if (!isset($score->{'score_id'})) {
                // Generate unique ID for a score if i can't get it from the API
                $score->{'score_id'} = substr(substr(strtotime($score->{'date'}), 3, 10) . $score->{'user_id'} . $score->{'beatmap_id'}, 0, 18);
            }
            $this->addScore($id, $score, $beatmap_id, $map["diff_size"]);
        }
    }

    public function getPersonalBests($id, $mode = "both", $top = 100)
    {
        $t = "";
        if ($top != -1) {
            $t = " LIMIT $top";
        }
        if ($mode == "both") {
            return $this->fast("SELECT * FROM ps_score_pb as p, ps_score as s, osu_allbeatmaps as o WHERE p.map = o.beatmap_id AND p.map = s.map AND id = :id AND p.mods = s.mods AND p.pp>=0 AND p.score_id = s.score_id AND (keycount = 4 OR keycount = 7) ORDER BY p.pp DESC$t", [":id" => $id]);
        } else {
            return $this->fast("SELECT * FROM ps_score_pb as p, ps_score as s, osu_allbeatmaps as o WHERE p.map = o.beatmap_id AND p.map = s.map AND id = :id AND p.mods = s.mods AND p.pp>=0 AND p.score_id = s.score_id AND keycount = :mode ORDER BY p.pp DESC$t", [":id" => $id, ":mode" => $mode]);
        }
    }

    public function getPersonalBestsMin($id)
    {
        return $this->fast("SELECT map,mods,score,pp FROM ps_score_pb WHERE id = :id", [":id" => $id]);
    }

    public function addPersonalBest($id, $map, $score_id, $mods, $score, $pp, $keys)
    {
        $opt = [
            ":score_id" => $score_id,
            ":map" => $map,
            ":id" => $id,
            ":mods" => $mods,
            ":score" => $score,
            ":pp" => $pp,
            ":keycount" => $keys,
        ];
        
        return $this->fast("INSERT INTO ps_score_pb (score_id, map, id, mods, score, pp, keycount) VALUES (:score_id, :map, :id, :mods, :score, :pp, :keycount) ON DUPLICATE KEY UPDATE score_id = IF(pp < :pp, :score_id, score_id), score = IF(pp < :pp, :score, score), pp = IF(pp < :pp, :pp, pp)", $opt);
    }

    public function addScore($id, $score, $beatmap_id = 0, $keycount)
    {
        $total_acc = ($score->{'count300'} + $score->{'countgeki'}) + ($score->{'countkatu'} * (2 / 3)) + ($score->{'count100'} * (1 / 3)) + ($score->{'count50'} * (1 / 6));

        $accuracy = $total_acc / ($score->{'count300'} + $score->{'countgeki'} + $score->{'countkatu'} + $score->{'count100'} + $score->{'count50'} + $score->{'countmiss'});

        if (!isset($score->{'beatmap_id'}) && $beatmap_id != 0) {
            $score->{'beatmap_id'} = $beatmap_id;
        }
        $opt = [
            ":map" => $score->{'beatmap_id'},
            ":user" => $score->{'user_id'},
            ":score" => $score->{'score'},
            ":scorid" => $score->{'score_id'},
            ":count300m" => $score->{'countgeki'},
            ":count300" => $score->{'count300'},
            ":count200" => $score->{'countkatu'},
            ":count100" => $score->{'count100'},
            ":count50" => $score->{'count50'},
            ":countmiss" => $score->{'countmiss'},
            ":combo" => $score->{'maxcombo'},
            ":mods" => $score->{'enabled_mods'},
            ":accuracy" => $accuracy,
            ":grade" => $score->{'rank'},
        ];

        if (!isset($score->{'pp'})) {

            $score_rep = [
                "score" => $score->{'score'},
                "count300m" => $score->{'countgeki'},
                "count300" => $score->{'count300'},
                "count200" => $score->{'countkatu'},
                "count100" => $score->{'count100'},
                "count50" => $score->{'count50'},
                "countmiss" => $score->{'countmiss'},
                "mods" => $score->{'enabled_mods'},
                "combo" => $score->{'maxcombo'},
            ];
            $opt[":pp"] = $this->getPP($score->{'user_id'}, $score->{'beatmap_id'}, $score_rep);
        } else {
            $opt[":pp"] = $score->{'pp'};
        }

        $r = "INSERT INTO ps_score 
        (`map`,`user`,`score`,`date`,`count300m`,`count300`,`count200`,`count100`,`count50`,`countmiss`,`combo`,`mods`,`accuracy`,`grade`,`score_id`,`pp`)
        VALUES
        (:map,:user,:score,\"" . $score->{'date'} . "\",:count300m,:count300,:count200,:count100,:count50,:countmiss,:combo,:mods,:accuracy,:grade,:scorid,:pp)";

        $this->addPersonalBest($score->{'user_id'}, $score->{'beatmap_id'}, $score->{'score_id'}, $score->{'enabled_mods'}, $score->{'score'}, $opt[":pp"], $keycount);

        return $this->fast($r, $opt);
    }

    public function addRecent($id)
    {
        $scores = $this->api->getUserRecent($id);
        foreach($scores as $score) {
            $score_rep = [
                "score" => $score->{'score'},
                "count300m" => $score->{'countgeki'},
                "count300" => $score->{'count300'},
                "count200" => $score->{'countkatu'},
                "count100" => $score->{'count100'},
                "count50" => $score->{'count50'},
                "countmiss" => $score->{'countmiss'},
                "mods" => $score->{'enabled_mods'},
                "combo" => $score->{'maxcombo'},
            ];
            $score->{"pp"} = $this->getPP($score->{'user_id'}, $score->{'beatmap_id'}, $score_rep);
        }
        $this->addScores($id, $scores);
        $this->setUpdated($id);
    }

    public function updatePBpp($user, $map,$pp) {
        $opt = [
            ":pp" => $pp,
            ":user" => $user,
            ":map" => $map,
        ];
        $r = "UPDATE ps_score_pb SET pp = :pp WHERE id = :user AND map = :map";

        $this->fast($r, $opt);
    }

    public function addBest($id)
    {
        $this->addScores($id, $this->api->getUserBest($id), 0, true);
        $this->setUpdated($id);
    }

    public function setUpdated($id) {
        $this->fast("UPDATE ps_user SET last_update = \"".date("Y-m-d H:i:s")."\" WHERE id = $id");
    }

    public function getUncalcScores($id = -1)
    {
        $c = "";
        if ($id != -1) {
            $c = " AND id = $id";
        }
        return $this->fast("SELECT * FROM ps_score WHERE pp == -1$c");
    }

    public function recalcPPAll($mode)
    {
        $users = $this->fast("SELECT id FROM ps_user");

        foreach ($users as $user) {
            $this->recalcPP($user["id"], $mode);
        }
    }

    public function recalcPP($id, $mode, $verbose = false, $bonus_pp = false)
    {
        
        $total = 0;
        $n = 0;

        $pbs = $this->getPersonalBests($id, $mode, 100);

        foreach ($pbs as $pb) {
            if ($pb["pp"] != -1) {
                $total += $pb["pp"] * pow(0.95, $n);
                $n++;
            }
        }

        // Bonus PP. Disabled, but still available if needed..
        if($bonus_pp) {
            $count = $this->getPBCount($id, $mode);
            $bonus = (417.0 - 1.0 / 3.0) * (1 - pow(0.9994, $count));
            $total += $bonus;
        }

        $this->fast("UPDATE ps_user SET pp$mode = $total WHERE id = $id");

        if ($verbose) {
            exec("echo '$id : $mode -> $total (".count($pbs)." scores)' >> /var/track/api/osu_api/refresh.log");
        }
    }

    public function recalcRank($mode)
    {
        $this->fast("SET @r=0;UPDATE ps_user SET rank$mode= @r:= (@r+1) ORDER BY pp$mode DESC;");
    }

    public function recalcCountry($mode, $country)
    {
        $this->fast("SET @r=0;UPDATE ps_user SET rank_country$mode= @r:= (@r+1) WHERE country=$country ORDER BY pp$mode DESC;");
    }

    public function importScoresFromOsudaily($id, $mode)
    {
        $scores = $this->fast("SELECT * FROM osu_score as s, osu_allbeatmaps as b WHERE s.beatmap_id = b.beatmap_id AND b.diff_size = $mode AND b.mode = 3 AND osu_id = $id AND (approved = 1 OR approved = 2)");

        $current_scores = $this->getPersonalBests($id);
        $personal_bests = [];
        foreach ($current_scores as $pb) {
            if (!isset($personal_bests["" . $pb["map"]])) {
                $personal_bests["" . $pb["map"]] = [];
            }
            $personal_bests["" . $pb["map"]][$pb["mods"]] = $pb["score"];
        }

        foreach ($scores as $score) {
            $grade = "F";
            if ($score["acc"] == 100) {
                $grade = "SS";
            } else if ($score["acc"] >= 95) {
                $grade = "S";
            } else if ($score["acc"] >= 90) {
                $grade = "A";
            } else if ($score["acc"] >= 80) {
                $grade = "B";
            } else if ($score["acc"] >= 70) {
                $grade = "C";
            } else if ($score["acc"] >= 45) {
                $grade = "D";
            }
            $score_id = substr(substr(strtotime($score["submitted"]), 3, 10) . $score["osu_id"] . $score["beatmap_id"], 0, 18);
            $req = "INSERT INTO ps_score 
            (`map`,`user`,`score`,`date`,`count300m`,`count300`,`count200`,`count100`,`count50`,`countmiss`,`combo`,`mods`,`accuracy`,`grade`,`score_id`,`pp`)
            VALUES
            (" . $score["beatmap_id"] . "," . $score["osu_id"] . "," . $score["score"] . ",\"" . $score["submitted"] . "\"," . $score["count300m"] . "," . $score["count300"] . ","
                . $score["count200"] . "," . $score["count100"] . "," . $score["count50"] . "," . $score["countmiss"] . "," . $score["maxcombo"] . "," . $score["mods"] . "," . $score["acc"] / 100
                . ",'$grade',$score_id,-1)";


            if (!isset($personal_bests["" . $score["beatmap_id"]]["" . $score["mods"]]) || $personal_bests["" . $score["beatmap_id"]]["" . $score["mods"]] < $score["score"]) {
                $this->addPersonalBest($score["osu_id"], $score["beatmap_id"], $score_id, $score["mods"], $score["score"], -1, $score["diff_size"]);
                $personal_bests["" . $score["beatmap_id"]]["" . $score["mods"]] = $score["score"];
            }

            $this->fast($req);
        }
    }

    public function getPBCount($id, $mode = "both")
    {
        if ($mode == "both") {
            return $this->fast("SELECT COUNT(*) as c FROM ps_score_pb as p WHERE (keycount = 4 = 4 OR keycount = 4 = 7) AND id = :id", [":id" => $id])[0][0];
        } else {
            return $this->fast("SELECT COUNT(*) as c FROM ps_score_pb as p WHERE keycount = :mode AND id = :id", [":id" => $id, ":mode" => $mode])[0][0];
        }
    }

    public function newUser($id, $string = 0)
    {
        $osu_user = $this->api->getUser($id, $string);
        $country_id = $this->fast("SELECT id FROM osu_country WHERE short=" . "'" . $osu_user->{"country"} . "'")[0][0];

        if ($country_id) {

            $opt = [
                ":id" => $osu_user->{"user_id"},
                ":username" => $osu_user->{"username"},
                ":country" => $country_id,
            ];

            $this->fast("INSERT INTO ps_user (id, username, country) VALUES (:id, :username, :country)", $opt);

            $this->refreshUser($id, $osu_user);

            return [$country_id, $osu_user->{"user_id"}];
        } else {
            echo "Country not recognized";
            return null;
        }
    }

    public function refreshUser($id, $cachedRequest = false)
    {
        if (!$cachedRequest) {
            $osu_user = $this->api->getUser($id);
        } else {
            $osu_user = $cachedRequest;
        }

        $opt = [
            ":pp" => $osu_user->{'pp_raw'},
            ":rank" => $osu_user->{'pp_rank'},
            ":rank_country" => $osu_user->{'pp_country_rank'},
            ":id" => $osu_user->{'user_id'},
        ];

        $this->fast("UPDATE ps_user SET pp = :pp, rank = :rank, rank_country = :rank_country WHERE id = :id", $opt);
    }

    public function refreshScoresUser($id, $pb = false, $latest = false)
    {
        if ($latest) {
            $this->addRecent($id);
        }
        if ($pb) {
            $this->addBest($id);
        }
        $this->setUpdated($id);
        $this->recalcPP($id, 4, true);
        $this->recalcPP($id, 7, true);
    }

    public function refreshEveryone($pb = false, $latest = false, $users = null, $log = false, $threadnum = 0)
    {
        if($users == null) {
            $users = $this->fast("SELECT id FROM ps_user");
        }

        $size = count($users);

        if($log) {
            echo $threadnum . " -> Starting for ".$size." users.\n";
        }

        $current = 0;

        if($log) {
            echo $threadnum . " -> Scores refresh.\n";
        }
        foreach ($users as $user) {
            $this->refreshScoresUser($user["id"], $pb, $latest);
            $current++;

            if($log && $current%10 == 0) {
                echo $threadnum . " -> ". $current."/".$size.".\n";
            }
        }


        if($log) {
            echo $threadnum . " -> Ranks refresh.\n";
        }
        $this->recalcRank(4);
        $this->recalcRank(7);

        $countries = $this->fast("SELECT id FROM osu_country");


        if($log) {
            echo $threadnum . " -> Country rank refresh.\n";
        }
        foreach ($countries as $country) {
            $this->recalcCountry(4, $country["id"]);
            $this->recalcCountry(7, $country["id"]);
        }
    }

    public function updateRank($mode, $id, $rank)
    {
        $this->fast("UPDATE ps_user SET rank$mode = $rank WHERE id = " . $id);
    }

    public function getPP($user, $beatmap_id, $score_rep = NULL, $mode = 3, $mods = 0)
    {
        // get $pp from parameter variables
        include("/var/track/api/osu_api/getPP.php");
        if (!isset($pp)) {
            $pp = 0;
        }
        return $pp;
    }

    public function getRate($since = 84600)
    {
        $req = "SELECT date,n FROM track_api_usage WHERE date >=\"" . date("Y-m-d H:i:00", time() - $since) . "\" ORDER BY date ASC";
        return $this->fast($req);
    }

    public function getRateGraph($since = 86400, $interval = 60, $groupfactor = 1, $divide = 60)
    {
        $group = $interval * $groupfactor;

        $results = $this->getRate($since);

        $data = [
            "x" => [],
            "y" => [],
            "type" => "lines"
        ];

        $now = strtotime(date("Y-m-d H:i", time()));
        $time = $now - $since;

        $currentT = 0;
        $currentR = 0;
        foreach ($results as $result) {
            $time += $interval;
            $i = 0;
            while (strtotime($result["date"]) > $time) {
                $i++;
                if ($i == $groupfactor) {
                    $data["x"][] = date("Y-m-d H:i", $time);
                    $data["y"][] = 0;
                    $i = 0;
                }
                $time += $interval;
            }
            if (strtotime($result["date"]) <= $time) {
                $currentR += $result["n"];
                $currentT += $interval;
                $time = strtotime($result["date"]);
            }
            if ($group <= $currentT) {
                $lastT = $currentT;
                $data["x"][] = date("Y-m-d H:i", $time);
                $data["y"][] = number_format($currentR / $divide, 2);

                $currentR = 0;
                $currentT = 0;
            }
        }
        while ($time < $now) {
            $data["x"][] = date("Y-m-d H:i", $time);
            $data["y"][] = 0;
            $time += $group;
        }

        $datas[] = $data;
        return json_encode($datas);
    }

    public function getLeaderboard($mode, $page = 1, $limit = 50)
    {
        $r = "SELECT id,username,country,rank$mode as pprank,pp$mode as performance FROM ps_user ORDER BY pp$mode DESC";
        $res = $this->fast($r);
        // Memes
        // foreach($res as $k => $s) {
        //     if($s["username"] == "Cunu") {
        //         $res[$k]["username"] = "Cunu The French Deglingod";
        //         $res[$k]["rank"] = 0;
        //         $res[$k]["performance"] = 12000000;
        //     }
        // }
        return $res;
    }

    public function addBeatmapScores($beatmap, $mods = [0, 64, 576])
    {
        foreach ($mods as $mod) {
            foreach ($this->api->getBeatmapScores($beatmap, $mod) as $score) {
                $user_req = $this->getUserInfo($score->{'username'}, 1);
                if ($user_req) {
                    $user_id = $user_req[0]["id"];
                    $this->addScores($user_id, [$score], $beatmap);
                }
            }
        }
    }

    public function getBeatmaps($min = false, $scanned = true, $cond = "")
    {
        $s = "";
        if (!$scanned) {
            $s = " AND beatmap_id NOT IN (SELECT beatmap_id FROM ps_beatmap_scanned WHERE DATE(`date`) = CURDATE())";
        }
        $selected = "*";
        if ($min) {
            $selected = "beatmap_id";
        }
        return $this->fast("SELECT $selected FROM osu_allbeatmaps WHERE mode=3 AND (approved = 1 || approved = 2) AND (diff_size = 4 || diff_size = 7)$cond $s");
    }

    public function getCompletion($min = 0, $max = 100, $mode = 4) {
        $spreads = [];
        $users = $this->fast("SELECT id FROM ps_user ORDER BY pp$mode DESC LIMIT $max");
        $i = 1;
        foreach($users as $user) {
            $pps = $this->fast("SELECT pp FROM ps_score_pb as p, ps_score as s WHERE p.score_id = s.score_id AND id = ".$user["id"]." ORDER BY pp DESC LIMIT 100");
            $spreads[] = $pps;
            $i++;
        }

        $datas = [];

        foreach($spreads as $s) {
            $d = [
                "x" => [],
                "y" => [],
                "type" => "lines"
            ];
            $i = 1;
            foreach($s as $pp) {
                $d["x"][] = $i;
                $d["y"][] = $pp[0];
                $i++;
            }

            $datas[] = $d;
        }

        return json_encode($datas);
    }
}
