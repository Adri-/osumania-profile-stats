<?php

require_once("../TrackDataManager.php");

class DataManager {

    public $database;

    public $api_secret = file_get_contents("/var/osu/api");

    public $request_last;
    public $requests_this_sec;
    
    public $rate_limit = 5;

    public function __construct() {

        $this->dm = new DataManager();
    }

    public function fast($req, $opt = []) {
        return $this->database->fast($req, $opt);
    }

    public function doRequest($scope, $r = "") {
        $t = time();

        if($this->request_last == $t){
            $this->requests_this_sec++;
        } else {
            $this->request_last = $t;
            $this->requests_this_sec = 1;
        }

        if($this->requests_this_sec > $this->rate_limit) {
            sleep(1);
        }

        $adress = "https://osu.ppy.sh/api/$scope?k=".$this->api_secret."&m=3$r";

        return json_decode(file_get_contents($adress));
    }
    
    public function getUserBest($id, $limit = 100) {
        if(!is_numeric($id)) {
            return [];
        }
        return $this->doRequest("get_user_best","&u=$id&limit=$limit");
    }
    
    public function getUserRecent($id, $limit = 50) {
        if(!is_numeric($id)) {
            return [];
        }
        return $this->doRequest("get_user_recent","&u=$id&limit=$limit");
    }
    
    public function addBest($id) {
        $this->dm->addScores($this->getUserBest($id));
    }
    
    public function addRecent($id) {
        $this->dm->addScores($this->getUserRecent($id));
    }
    
}
