<?php
require_once '/var/track/api/lib/TrackDataManager.php';
$dm = new DataManager();
$c = 0;
$users = $dm->getUsersMin() ;
$cu = count($users);

foreach($users as $user) {
    $c++;
    echo "$c/$cu\n";
    
    $uscores = $dm->getAllUserScores($user["id"],4);

    foreach($uscores as $score) {
        $dm->addPersonalBest($score["user"], $score["map"], $score["score_id"], $score["mods"], $score["score"], $score["pp"],4);
    }
    
    $uscores = $dm->getAllUserScores($user["id"],7);

    foreach($uscores as $score) {
        $dm->addPersonalBest($score["user"], $score["map"], $score["score_id"], $score["mods"], $score["score"], $score["pp"],7);
    }
}