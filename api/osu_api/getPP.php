<?php

$LOGGING = false;
if (isset($_REQUEST["verbose"])) {
    $LOGGING = true;
}

$more = false;
if (isset($_REQUEST["more"])) {
    $more = true;
}

if (!function_exists("_log")) {

    function _log($message)
    {
        global $LOGGING;
        if ($LOGGING) {
            echo $message;
        }
    }
}

require_once '/var/track/api/lib/TrackDataManager.php';


if (isset($user) && isset($beatmap_id) && isset($mode) && isset($mods)) {
    $_REQUEST["u"] = $user;
    $_REQUEST["b"] = $beatmap_id;
    $_REQUEST["m"] = $mode;
    $_REQUEST["mods"] = $mods;
} else {
    $last = file_get_contents("/var/osu/pp_last");
    if (strtotime(date("Y-m-d H:i:s")) - strtotime($last) <= 6) {
        echo "Only 1 request every 6 seconds is allowed.";
        die();
    }
}

$dm = new DataManager();

if (!isset($_REQUEST["u"]) || !is_numeric($_REQUEST["u"])) {
    echo "No user specified.";
    die();
} else {
    $u = $_REQUEST["u"];
}
if (!isset($_REQUEST["b"]) || !is_numeric($_REQUEST["b"])) {
    echo "No beatmap specified.";
    die();
} else {
    $b = $_REQUEST["b"];
}
if (!isset($_REQUEST["mods"]) || !is_numeric($_REQUEST["mods"])) {
    $usermods = 0;
} else {
    $usermods = $_REQUEST["mods"];
}

if (!isset($_REQUEST["m"]) || !is_numeric($_REQUEST["m"])) {
    $m = 0;
} else {
    $m = $_REQUEST["m"];
}

$req = "SELECT * FROM osu_allbeatmaps WHERE beatmap_id = $b";
$beatmap_metadata = $dm->fast($req);
if (!$beatmap_metadata) {
    echo "$b doesn't exist\n";
}
$beatmap_metadata = $beatmap_metadata[0];

if (!$beatmap_metadata) {
    echo "Unknown beatmap.\n";
} else {
    $keys = $beatmap_metadata["diff_size"];
    $m_m = $beatmap_metadata["mode"];
    switch ($m_m) {
        case 0:
            $mode = "std";
            break;
        case 1:
            $mode = "taiko";
            break;
        case 2:
            $mode = "catch";
            break;
        case 3:
            $mode = "mania";
            break;
    }
    $r_m = $_REQUEST["m"];
    switch ($_REQUEST["m"]) {
        case 0:
            $r_m = "std";
            break;
        case 1:
            $r_m = "taiko";
            break;
        case 2:
            $r_m = "catch";
            break;
        case 3:
            $r_m = "mania";
            break;
    }

    if ($mode != "mania") { } else {
        $maps_dir = "/var/www/data/maps/$mode";

        if (!isset($score_rep) || $score_rep == NULL) {
            $req = "SELECT * FROM ps_score WHERE map = $b AND user = $u AND mods = $usermods";
            $score_rep = $dm->fast($req)[0];
        }

        $bin = strrev(decbin($score_rep["mods"]));

        $hh = fopen("/var/osu/pp_last", "w+");
        fwrite($hh, date("Y-m-d H:i:s"));

        if ($m_m != $m) {
            $file = "$maps_dir/$b.osu.$r_m";
            $converted = true;
        } else {
            $file = "$maps_dir/$b.osu";
            $converted = false;
        }

        if (!is_file($file)) {
            $beatmap_file_name = $b . ".osu";
            $beatmap_id = $b;
            $output_dir = $maps_dir;
            require "/var/osu/addOsuMap.php";
            $file = "$maps_dir/$beatmap_file_name";
        }
        $ppcommand = "dotnet /var/track/getPP/getPP/bin/Release/netcoreapp2.1/debian.9-x64/getPP.dll \"$file\" "
            . $score_rep["score"] . " "
            . $score_rep["count300m"] . " "
            . $score_rep["count300"] . " "
            . $score_rep["count200"] . " "
            . $score_rep["count100"] . " "
            . $score_rep["count50"] . " "
            . $score_rep["countmiss"] . " "
            . $score_rep["mods"] . " "
            . $score_rep["combo"];

        $pp = shell_exec($ppcommand);
    }
}
