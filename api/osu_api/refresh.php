<?php
require_once '/var/track/api/lib/TrackDataManager.php';
$dm = new DataManager();

$simult = 10;

$d = date("Y-m-d H:i:s", time() - 5600 );
$r = "SELECT id FROM ps_user WHERE last_update < '$d'";

$users = $dm->fast($r);
$usersCount = count($users);

if ($usersCount > $simult) {
    echo "Refreshing $usersCount users.\n\n";

    $chunks = array_chunk($users, $usersCount / $simult);

    $workersData = [];
    $c = 0;
    foreach ($chunks as $set) {
        $workersData[$c] = $set;
        $c++;
    }

    file_put_contents("workersRefreshData.json", json_encode($workersData));

    exec("echo '' > refresh.log");
    for ($i = 0; $i < $simult; $i++) {
        exec("php workerRefresh.php $i > refresh.log 2>/dev/null &");
    }
}

echo "Done\n";
