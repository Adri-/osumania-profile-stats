<?php
require_once '/var/track/api/lib/TrackDataManager.php';

$dm = new DataManager();

$simult = 6;

$beatmaps = $dm->getBeatmaps(true,false, " AND difficultyrating > 3");
$beatmapsCount = count($beatmaps);

echo "Scanning $beatmapsCount beatmaps.\n\n";

$chunks = array_chunk($beatmaps, $beatmapsCount /$simult);

$workersData = [];
$c = 0;
foreach($chunks as $set) {
    $workersData[$c] = $set;
    $c++;
}

file_put_contents("workersData.json",json_encode($workersData));

exec("echo '' > scores.log");
for($i = 0; $i < $simult;$i++) {
    echo shell_exec("php worker.php $i > /dev/null 2>&1 &");
}

