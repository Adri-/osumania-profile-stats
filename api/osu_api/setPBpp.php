<?php
require_once '/var/track/api/lib/TrackDataManager.php';
$dm = new DataManager();
$c = 0;
$users = $dm->getUsersMin() ;
$cu = count($users);

foreach($users as $user) {
    $c++;
    echo "$c/$cu\n";
    $bests = $dm->getPersonalBests($user["id"], "both", -1);
    
    $uscores = $dm->getAllUserScores($user["id"]);
    $getscore = [];

    foreach($uscores as $us) {
        if(!isset($getscore[$us["map"].""])) {
            $getscore[$us["map"].""] = $us;
        } else {
            if($getscore[$us["map"].""]["pp"] < $us["pp"]) {
                $getscore[$us["map"].""] = $us;
            }
        }
    }

    foreach($bests as $b) {
        $score = $getscore[$b["map"].""];
        if($score["pp"] != $b["pp"]) {
            $dm->updatePBpp($b["id"],$b["map"],$score["pp"]);
            echo $b["id"] . " > " . $score["pp"] . " (was ".$b["pp"].")\n";
        }
    }
}