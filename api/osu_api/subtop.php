<?php
require_once '/var/track/api/lib/TrackDataManager.php';
$dm = new DataManager();

$mode = 3;
$pages = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
            21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40];

error_reporting(E_ERROR);

// Find top users on the ranking page since it's not available in the api
foreach ($pages as $page) {
    echo "\nPage $page\n";

    $c = curl_init("https://old.ppy.sh/p/pp/?m=$mode&page=$page");
    curl_setopt($c, CURLOPT_RETURNTRANSFER, true);


    $html = curl_exec($c);

    if (curl_error($c))
        die(curl_error($c));

    $status = curl_getinfo($c, CURLINFO_HTTP_CODE);

    $doc = new DOMDocument();
    $doc->loadHTML($html);

    $newname = false;
    $usernames = [];

    $tds = $doc->getElementsByTagName('td');
    foreach ($tds as $td) {
        if ($newname) {
            array_push($usernames, $td->nodeValue);
            $newname = false;
        }
        if ($td->nodeValue[0] == "#") {
            $newname = true;
        }
    }

    curl_close($c);

    foreach ($usernames as $user) {
        echo $user . " - ";

        $result = "none";
        $info = $dm->getUserInfo($user,1);

        if ($info) {
            echo "Already registred";
        } else {
            $info = $dm->newUser($user, 1); // Username string
            echo " Created - ";

            if ($info != null) {
                //$dm->importScoresFromOsudaily($info[1], 4);
                //$dm->importScoresFromOsudaily($info[1], 7);

                $dm->addRecent($info[1]);
                echo " Recent - ";
                $dm->addBest($info[1]);
                echo " Best - ";

                $dm->recalcPP($info[1], 4);
                $dm->recalcPP($info[1], 7);
                echo " PP - ";

                $dm->recalcRank(4);
                $dm->recalcRank(7);
                echo " Rank - ";

                $dm->recalcCountry(4, $info[0]);
                $dm->recalcCountry(7, $info[0]);

                echo "Success";
            } else {
                echo "No API result";
            }
        }
        echo "\n";
    }
}
