<?php
require_once '/var/track/api/lib/TrackDataManager.php';
$dm = new DataManager();

$maps = $dm->database->fast("SELECT DISTINCT map FROM ps_score_pb");

foreach($maps as $map) {
    $keys = $dm->database->fast("SELECT diff_size FROM osu_allbeatmaps WHERE beatmap_id = ".$map["map"] . " AND mode = 3");

    if(!$keys) {
        echo $map["map"] . " convert\n";
        continue;
    }
    $keys = $keys[0][0];

    echo $map["map"] . " - ".  $keys . "K\n";
    $dm->database->fast("UPDATE ps_score_pb SET keycount = $keys WHERE map = ".$map["map"]);
}