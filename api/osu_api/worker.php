<?php

require_once '/var/track/api/lib/TrackDataManager.php';
$dm = new DataManager();

$beatmaps = json_decode(file_get_contents("workersData.json"))[$argv[1]];

$c = 0;
foreach($beatmaps as $beatmap) {
    $c++;
    exec("echo '$c' >> scores.log");
    $dm->addBeatmapScores($beatmap->{"beatmap_id"});
    $dm->fast("INSERT INTO ps_beatmap_scanned (`beatmap_id`) VALUES (" . $beatmap->{"beatmap_id"} . ") ON DUPLICATE KEY UPDATE `date` = NOW()");
}
