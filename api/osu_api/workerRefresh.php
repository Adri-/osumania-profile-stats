<?php

require_once '/var/track/api/lib/TrackDataManager.php';
$dm = new DataManager();

$usersData = json_decode(file_get_contents("workersRefreshData.json"))[$argv[1]];
$users = [];
foreach($usersData as $u) {
    $users[] = ["id" => $u->{'id'}];
}

$dm->refreshEveryone(true,true, $users, true, $argv[1]);