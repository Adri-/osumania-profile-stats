<?php
require_once '/var/track/api/lib/TrackDataManager.php';
$dm = new DataManager();

$divide = 60;
$interval = 60;
$groupfactor = 1;
$since = 86400;

?>
<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script src="https://cdn.plot.ly/plotly-latest.min.js"></script>
</head>
<div style='text-align: center;margin-top:50px;margin-top: 70px;'>
    <div class='bodycontainer' style='width:70%;margin-right:auto;margin-left:auto;'>
        <br>
        <p style='font-size:220%'>API usage</p>
        <div id='graphusage' style='width: 98%; height: 400px;margin: auto auto auto auto;'></div>
    </div>

    <script>
        function createGraphs() {
                Plotly.newPlot('graphusage', <?php echo $dm->getRateGraph($since, $interval, $groupfactor, $divide); ?>);
        }
    </script>

    <script>
        $(document).ready(function () {
            createGraphs();
        })
    </script>
</div>