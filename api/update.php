<?php
header('Access-Control-Allow-Origin: *');

if (!isset($_REQUEST["k"])) {
    echo json_encode(["missing_param" => "You need to provide an API key."]);
    die();
}
if (!isset($_REQUEST["u"]) || !is_numeric($_REQUEST["u"])) {
    echo json_encode(["missing_param" => "You need to provide a user ID."]);
    die();
}

include './lib/TrackAPIManager.php';

$api = new APIManager();

$access = $api->authorize($_REQUEST["k"], "users");

if (isset($access["error"])) {
    echo json_encode($access);
    die();
}

$dm = new DataManager();
$info = $dm->getUserInfo($_REQUEST["u"])[0];

if ($info) {
    $dm->addRecent($_REQUEST["u"]);
    $dm->recalcPP($_REQUEST["u"], 4);
    $dm->recalcPP($_REQUEST["u"], 7);
    $dm->recalcRank(4);
    $dm->recalcRank(7);
    $dm->recalcCountry(4, $info["country"]);
    $dm->recalcCountry(7, $info["country"]);

    echo json_encode(["success" => "done."]);
} else {
    echo json_encode(["error" => "unknown user."]);
}
