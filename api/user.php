<?php
header('Access-Control-Allow-Origin: *');

function stringifyMods($number) {
        $number = intval($number);
        $mods = "";
    
        if($number & 1<<0) $mods .= 'NF';
        if($number & 1<<1) $mods .= 'EZ';
        if($number & 1<<3) $mods .= 'HD';
        if($number & 1<<4) $mods .= 'HR';
        if($number & 1<<5) $mods .= 'SD';
        if($number & 1<<9) $mods .= 'NC';
        else if($number & 1<<6) $mods .= 'DT';
        if($number & 1<<7) $mods .= 'RX';
        if($number & 1<<8) $mods .= 'HT';
        if($number & 1<<10) $mods .= 'FL';
        if($number & 1<<12) $mods .= 'SO';
        if($number & 1<<14) $mods .= 'PF';
        if($number & 1<<15) $mods .= '4k';
        if($number & 1<<16) $mods .= '5k';
        if($number & 1<<17) $mods .= '6k';
        if($number & 1<<18) $mods .= '7k';
        if($number & 1<<19) $mods .= '8k';
        if($number & 1<<20) $mods .= 'FI';
        if($number & 1<<24) $mods .= '9k';
        if($number & 1<<25) $mods .= '10k';
        if($number & 1<<26) $mods .= '1k';
        if($number & 1<<27) $mods .= '3k';
        if($number & 1<<28) $mods .= '2k';
    
        return $mods;
}

if(!isset($_REQUEST["k"])) {
    echo json_encode(["missing_param" => "You need to provide an API key."]);
    die();
}
if(!isset($_REQUEST["u"])) {
    echo json_encode(["missing_param" => "You need to provide a user ID."]);
    die();
}

include './lib/TrackAPIManager.php';

$api = new APIManager();

$access = $api->authorize($_REQUEST["k"], "users");

if(isset($access["error"])) {
    echo json_encode($access);
    die();
}

$dm = new DataManager();

$data = $dm->getUserFull($_REQUEST["u"])[0];
$pb_4k = $dm->getPersonalBests($_REQUEST["u"], 4, 100);
$pb_7k = $dm->getPersonalBests($_REQUEST["u"], 7, 100);

foreach($pb_4k as $pb) {
    $pb["title"] = $pb["title"] . "[" + $pb["version"] + "]";

    if($pb["mods"]) {
        $pb["title"] .= " + " . stringifyMods($pb["mods"]);
    }
}

foreach($pb_7k as $pb) {
    $pb["title"] = $pb["title"] . "[" + $pb["version"] + "]";

    if($pb["mods"]) {
        echo "mods";
        $pb["title"] .= " + " . stringifyMods($pb["mods"]);
    }
}

$userPublic = [
    "id" => $data["osu_id"],
    "username" => $data["username"],
    "country" => $data["short"],
    "pp4" => $data["pp4"],
    "rank4" => $data["rank4"],
    "rank_country4" => $data["rank_country4"],
    "pp7" => $data["pp7"],
    "rank7" => $data["rank7"],
    "rank_country7" => $data["rank_country7"],
    "personal_bests" => [
        4 => $pb_4k,
        7 => $pb_7k,
    ],
];

echo json_encode($userPublic); 