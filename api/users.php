<?php
header('Access-Control-Allow-Origin: *');

if(!isset($_REQUEST["k"])) {
    echo json_encode(["missing_param" => "You need to provide an API key."]);
    die();
}

include './lib/TrackAPIManager.php';

$api = new APIManager();

$access = $api->authorize($_REQUEST["k"], "user");

if(isset($access["error"])) {
    echo json_encode($access);
    die();
}

$dm = new DataManager();

echo json_encode($dm->getUsersMin());