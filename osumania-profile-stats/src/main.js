import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'

import Index from './components/Index'
import User from './components/User'
import numFormat from 'vue-filter-number-format';
 
Vue.config.productionTip = false

Vue.use(VueRouter);
Vue.filter('numFormat', numFormat);

const routes = [
  { path: '/', component: Index },
  { path: '/user/:id', component: User }
]
const router = new VueRouter({
  routes // short for `routes: routes`
})

new Vue({
  router: router,
  render: h => h(App),
}).$mount('#app')
